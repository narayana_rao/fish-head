<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');

// routers
Route::get('album-jobs/{id}', 'Controller@albumJobs')->name('album-jobs');
Route::get('jobs-images/{id}', 'Controller@jobImage')->name('jobs-images');

Route::resource('job', 'JobController');
Route::resource('image', 'ImageController');

Route::resource('album', 'AlbumController');
