<?php

namespace App\Http\Controllers;

use App\Album;
use Illuminate\Http\Request;

use App\Image;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['objImages'] = Image::all();
        return view('image.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['method'] = 'POST';
        $data['action'] = route('image.store');
        $data['objAlbums'] = Album::all();

        return view('image.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objImage = new Image();

        $path = '';
        if ($request->hasFile('image')) {
            $path = $request->image->store('images', 'public');
            $objImage->image = $path;
        }

        $objImage->message = $request->input('message');
        $objImage->display_message = $request->has('display_message') ? $request->input('display_message') : 0;
        $objImage->album_id = $request->input('album');

        $objImage->save();

        return redirect(route('image.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['objImage'] = Image::find($id);
        $data['objAlbums'] = Album::all();

        $data['method'] = 'PUT';
        $data['action'] = route('image.update', [$id]);

        return view('image.add', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objImage = Image::fine($id);

        $path = '';
        if ($request->hasFile('image')) {
            $path = $request->image->store('images', 'public');
            $objImage->image = $path;
        }

        $objImage->message = $request->input('message');
        $objImage->display_message = $request->has('display_message') ? $request->input('display_message') : 0;
        $objImage->album_id = $request->input('album');

        $objImage->save();

        return redirect(route('image.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
