<?php

namespace App\Http\Controllers;

use App\Album;
use App\Job;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {

        $data = [];
        $data['objAlbums'] = Album::paginate(2);

        return view('welcome', $data);
    }

    public function albumJobs($albomID)
    {
        $data['objJobs'] = Album::with('jobs')->find($albomID);

        return view('welcome', $data);
    }

    public function jobImage($jobID)
    {
        $data['objImages'] = DB::table('images')
            ->leftJoin('albums', 'images.album_id', '=', 'albums.id')
            ->leftJoin('jobs', 'jobs.album_id', '=', 'albums.id')
            ->where('jobs.id', $jobID)
            ->paginate(10);

        return view('welcome', $data);
    }
}
