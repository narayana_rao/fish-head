<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    public function albums()
    {
        return $this->belongsTo('App\Album');
    }

    public function images()
    {
        //return $this->hasManyThrough('App\Album', 'App\Image');
        return $this->morphOne('App\Image', 'imageable');
        //return $this->hasOneThrough('App\Image', 'App\Album');
    }
}
