<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    //
    public function jobs()
    {
        return $this->hasMany('App\Job');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }
}
