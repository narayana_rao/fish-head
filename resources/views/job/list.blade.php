@extends('layouts.template')

@section('container')
<div class="content">
    <div class="row">
        <a href="{{route('job.create')}}">Add</a>
        <table class="table" id="data-table">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Action</th>
            </thead>
            <tbody>
                @forelse($objJobs as $objJob)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$objJob->name}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{route('job.edit',[$objJob->id])}}"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-primary" href="javascript:void(0)" onclick="deleteRecord('{{route('job.destroy',[$objJob->id])}}', 'DELETE')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @empty
                Data Not Available
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection