@extends('layouts.template')

@section('container')
<form action="{{$action}}" method="POST">

    @method($method)
    @csrf

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ isset($objJob->name) ? $objJob->name : ''}} " placeholder="enter name">
    </div>

    <div class="form-group">
        <label for="album">Album</label>
        <select name="album" class="form-control">
            @forelse($objAlbums as $objAlbum)
            <option value={{$objAlbum->id}} {{isset($objJob) ? (($objAlbum->id == $objJob->album_id) ? 'selected' : '') : ''}}>{{$objAlbum->name}}</option>
            @empty
            <option>-- NONE-- </option>
            @endforelse
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>

</form>
@endsection