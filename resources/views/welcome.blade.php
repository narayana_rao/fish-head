@extends('layouts.template')

@section('container')

@isset($objAlbums)
<div class="row">
    @forelse($objAlbums as $objAlbum)
    <a href="{{route('album-jobs',[$objAlbum->id])}}">{{$objAlbum->name}}</a>
    @empty

    @endforelse
    {{$objAlbums->links()}}
</div>
@endisset

@isset($objJobs)
<div class="row">
    @forelse($objJobs->jobs as $objJob)
    <a href="{{route('jobs-images',[$objJob->id])}}">{{$objJob->name}}</a>
    @empty

    @endforelse
</div>
@endisset

@isset($objImages)

<div id="myCarousel" class="carousel slide" data-ride="carousel">


    <!-- Indicators -->
    <ul class="carousel-indicators">
        @forelse($objImages as $objImage)
        <li data-target="#myCarousel" data-slide-to="{{$loop->iteration}}" class="{{(!$loop->index) ? 'active' : ''}}   "></li>
        @empty

        @endforelse
    </ul>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        @forelse($objImages as $objImage)
        <div class="carousel-item {{(!$loop->index) ? 'active' : ''}}">
            <img src="{{asset('storage/'.$objImage->image)}}" alt="Chania">
            @if($objImage->display_message)
            <div class="carousel-caption">
                <p>{{$objImage->message}}</p>
            </div>
            @endif
        </div>
        @empty

        @endforelse
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
{{$objImages->links()}}
@endisset
@endsection