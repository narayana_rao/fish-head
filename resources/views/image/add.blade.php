@extends('layouts.template')

@section('container')
<form action="{{$action}}" method="POST" enctype='multipart/form-data'>
    @csrf

    @method($method)

    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" class="form-control" name="image">
    </div>

    <div class="form-group">
        <label for="message">Message</label>
        <input type="text" name="message" id="message" class="form-control" value="{{ isset($objImage->message) ? $objImage->message : ''}} " placeholder="enter message">
    </div>

    <div class="form-group">
        <label for="d-message">Disaplay message</label>
        <input type="checkbox" name="display_message" id="d-message" class="form-control" value="1">
    </div>

    <div class="form-group">
        <label for="album">Album</label>
        <select name="album" class="form-control">
            @forelse($objAlbums as $objAlbum)
            <option value={{$objAlbum->id}} {{isset($objImage) ? (($objAlbum->id == $objImage->album_id) ? 'selected' : '') : ''}}>{{$objAlbum->name}}</option>
            @empty
            <option>-- NONE-- </option>
            @endforelse
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>

</form>
@endsection