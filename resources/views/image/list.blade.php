@extends('layouts.template')

@section('container')
<div class="content">
    <div class="row">
        <a href="{{route('image.create')}}">Add</a>
        <table class="table" id="data-table">
            <thead>
                <th>#</th>
                <th>Name</th>
                <th>Action</th>
            </thead>
            <tbody>
                @forelse($objImages as $objImage)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$objImage->image}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{route('image.edit',[$objImage->id])}}"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-primary" href="javascript:void(0)" onclick="deleteRecord('{{route('image.destroy',[$objImage->id])}}', 'DELETE')"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @empty
                Data Not Available
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection