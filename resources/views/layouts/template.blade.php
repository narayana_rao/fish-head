<html>

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
</head>

<body>
    <header>
        <div class="row">
            <div class="col-lg-5 offset-lg-1">
                <a href="{{route('album.index')}}"> Album</a>
                <a href="{{route('job.index')}}"> Job</a>
                <a href="{{route('image.index')}}"> Image</a>
            </div>
        </div>
    </header>
    <div class="container">
        @yield('container')
    </div>
</body>
<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $("#data-table").DataTable();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function deleteRecord(url, method, data = []) {
            $.ajax({
                type: method,
                url: url,
                data: data,
                success: function(response) {
                    console.log(response);
                    location.reload();
                },
                error: function(response) {
                    console.log(response);
                }
            });
        }
        @section('script')

        @show
    </script>
</footer>

</html>