@extends('layouts.template')

@section('container')
<form action="{{$action}}" method="POST">
    @csrf

    @method($method)

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ isset($objAlbum->name) ? $objAlbum->name : ''}} " placeholder="enter name">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>

</form>
@endsection